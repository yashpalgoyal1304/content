Step-by-Step Video launched: How to Report an Issue in Inkscape in Gitlab



If you've ever come across issues in Inkscape and wondered about how to report them to developers who can fix them, we have you covered! 

Inkscape Project Board Member Chris Rogers has produced an easy-to-follow video on how to report Inkscape bugs in Gitlab, the online platform the project uses to track such things. 

As an Inkscape user,  here's your chance to put your skills to work and help improve the program. 

The video invites you to sign up for a Gitlab account and connect with Inkscape there at  walks you through some steps. 

Before reporting an issue, it's important to be able to reproduce the error and to capture it - save it as an SVG file. You'll find more details on how to do this in the video.

Once you're signed up on Gitlab, here are the next steps to follow: 

1. Log in to Gitlab by heading to www.inkscape.org/report 
2. Search for your issue (or let developers know you're also experiencing a known issue)
3. Create a new issue (to report your new issue)
4. Submit the new issue 

Thanks for your help! Together we can make Inkscape better for everyone! 



SOCIAL MEDIA POSTS

TWITTER / MASTODON

Help improve #Inkscape for everyone. Be part of the solution! Learn "How to Report an Issue in Inkscape in Gitlab" with step-by-step video (link to video on site)   



FACEBOOK

Hello Inkscapers! Would you like to help improve #Inkscape for everyone? Well, you can be part of the solution! 

Learn "How to Report an Issue in Inkscape in Gitlab" with this step-by-step video (link to video on site)

Thanks for helping to make Inkscape better for everyone.

#Inkscape  #OpenSource  #Gitlab  #Collaboration