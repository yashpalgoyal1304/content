# Inkscape 1.0 beta1 available for testing

The Inkscape project has released a first beta version of the upcoming and much-awaited Inkscape 1.0!

After releasing two less visible alpha versions this year, in [mid-January](https://inkscape.org/release/inkscape-1.0alpha0/) and [mid-June](https://inkscape.org/release/inkscape-1.0alpha2/), Inkscape is now ready for extensive testing and subsequent bug-fixing.

The most notable changes to watch out for are:

* [Theming support](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Theme_selection)
* [Origin in top left corner](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Y_Axis_Inversion)
* [Canvas rotation and mirroring](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Canvas_Rotation)
* [On-Canvas alignment of objects](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#On-Canvas_Alignment)
* [Better HiDPI screen support](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#HiDPI)
* [Controlling the width of PowerStroke with pressure sensitive graphics tablet](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#PowerPencil)
* [Fillet/chamfer LPE](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Fillet.2FChamfer_LPE) and (lossless) [Boolean Operation LPE](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Boolean_Operations_LPE)
* [New PNG export options](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Export_PNG_images)
* [Centerline tracing](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Trace_Bitmap)
* [New Live Path Effect selection dialog](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#General_2)
* [Faster Path operations and deselection of large number of paths](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Notable_Bugfixes)
* [Variable fonts support](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Variable_Font_Support)
* [Complete extensions overhaul](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Extensions)
* [Command line syntax changes](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Command_Line)
* [Native support for macOS with a signed and notarized .dmg file](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Mac_Application)

Read the draft release notes for Inkscape 1.0, which list more than 100 major and minor improvements and bug fixes since 0.92.4, in the [Inkscape Wiki](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0).

Before the final release, the project plans to create at least one other Beta version. Translations and documentation still need to be updated. A list of known issues to be worked on before 1.0 can be found [here](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Known_Issues).

Please test away and report your findings at [https://inkscape.org/report](https://inkscape.org/report)! We are especially interested in:

* problems with texts in Inkscape files from older Inkscape versions
* unknown crashes
* extensions not doing what they should be doing
* things that worked in 0.92.4, but are no longer working in the beta version 

If you are using Inkscape on the command line, please test [the new functionality](http://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Command_Line) and let us know if any issues come up for you. 

If you maintain a custom extension for Inkscape, please test it, and update it in time to be compatible with 1.0, so your users will be able to update their Inkscape installation together with your extension. 

And lastly, if you are fluent in graphics lingo, in both English and another language, please consider helping your favorite vector graphics editor by [updating translations for your language](http://wiki.inkscape.org/wiki/index.php/Interface_translation). 

Download Inkscape 1.0 beta0 from https://inkscape.org/release/inkscape-1.0beta1/ for your operating system (Linux, Windows, macOS) as soon as packagers have made their versions available.