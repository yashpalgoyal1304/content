
Inkscape Survey

The purpose of this ticket wold be to come up with a survey so Inkscape project maintainers would have a better idea on what is working, and what the project could try to improve.

The list of questions is the following (1st draft):

What is your gender?
    Male
    Female
    Non Binary

What is your age?
    01 - 13 years
    14 - 23 years
    24 - 33 years
    34 - 43 years
    44 - 53 years
    54 - 63 years
    64 - 75 years
    76+

What country are you from?
    Dropdown list of countries with full name written

What is the main language you consume Inkscape application?
    Dropdown list of languages inkscape UI has translations

What is your Operating System?
    Linux
    MS Windows
    macOS

How would you judge your own inkscape knowledge?
    Beginner (never used the application before)
    Intermediate (know my way around fairly well)
    Advanced (know how to use extensions, filters and XML editor)
    Hard core (know how to debug, write inkscape extensions, use inkscape from command line)

How often do you use inkscape?
    1x /week
    2x /week
    3x /week
    Daily
    1x /month
    Sporadic

What is your main use of inkscape (multiple choices)
    Design icons
    PCB (Print Circuit Board)
    Engineering
    Maps
    Logos
    Printed Media
    Digital/Web Media
    Create Fonts
    Laser Cutting
    Vinyl Cutting
    Sublimation

Do you subscribe to any mailing lists from the project?
    Announcements
    Board of Directors (by invite only)
    Devel
    Docs
    Infrastructure
    Packagers
    Packagers
    Translators
    Users
    Vectors (marketing)

Would you like to volunteer to be part of any of these teams (if you already are not)? If yes, which ones?
    Announcements
    Board of Directors (by invite only)
    Devel
    Docs
    Infrastructure
    Packagers
    Packagers
    Translators
    Users
    Vectors (marketing)

Have you used the forum before? (https://inkscape.org/forums/)
    Yes
    No

Would you consider donating money for specific features to be developed?
    Yes
    No

Would you be willing to contribute to the inkscape project?
    Yes
    No

How?
    Donating money
    Volunteering my free time to help the project

Would you recommend Inkscape to your friends (yes/no)? Why?
    Yes
    No

Why?
    Answer

Did you know that Inkscape is run entirely by volunteers in their spare time (as in: no company backing, no paid work)?
    Yes
    No

Would you be more encouraged to contribute to the project (in whatever form) if you knew more about what other people are contributing?
    Yes
    No

Have you ever contributed to any Software Freedom project? (I'm using "free" instead of "open source" on purpose because I have at least two examples where I donated to freeware that is not open source.)
    Yes
    No

Do you use Inkscape in a commercial context?
    Yes
    No

What communication methods do you use?
    Chat
    Forums
    Twitter
    Mailing list
    Gitlab
    None

Any final comments about Inkscape?
    Open ended answer.