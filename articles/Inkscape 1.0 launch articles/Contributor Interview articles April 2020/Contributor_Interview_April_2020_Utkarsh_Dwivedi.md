Inkscape 1.0 release - April 2020 interview

Informations sur la soumission

Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)

Soumis par Anonyme (non vérifié)

sam, 25/04/2020 - 23:45

0.0.0.0



**Contributor Interview: Utkarsh Dwivedi**



Headline 

**"I enjoy the feeling that I contributed to something so powerful as Inkscape "**



**Please introduce yourself; what is your name and where in the world do you live?**

Utkarsh Dwivedi, from Bhopal, India. (CyanideForBreakfast)



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

I came across it when I wanted to make SVG manipulations for my website.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I solved one bug : #1011 by !1543 - LPE Offset issue and updated the CONTRIBUTING.md: !1478.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

I think the community is extremely helpful and consists of so many talented and skillful contributors that I could learn so much from. I enjoy the feeling that I contributed to something so powerful as Inkscape that so many people are going to use in the future.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

Namely LPE Offset



**What's your favorite part of the 1.0 release?**

I loved the UI developments of 1.0.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

SVG animations. They are so very needed in websites.



________________

How can we contact you for details / clarifications?dwivedi.utkarsh01@gmail.com

