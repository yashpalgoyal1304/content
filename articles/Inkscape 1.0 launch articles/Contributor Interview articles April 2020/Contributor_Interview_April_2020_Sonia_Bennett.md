Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 23:13*

*0.0.0.0*



**Contributor Interview: Sonia Bennett**



Headline 

**"It's amazing to work with people from all over the world"**



**Please introduce yourself; what is your name and where in the world do you live?**

Hi! My name is Sonia Bennett. I'm originally from India and I currently live in the US.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

It was back in 2015 when I first discovered Inkscape, after years of devotion to the industry standard of illustration software, only to be left out in the cold by its new system of continued extortion. I joined the merry band of Facebook Inkscapers and never looked back!



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I'm on the admin team in the Inkscape group on Facebook and I help out by illustrating challenge topic visuals for the members. I've also recently helped with creating gifs for the release notes for 1.0.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

The Inkscape challenges way back in 2015, posted by Istvan Szep, really encouraged me to create art in Inkscape and to keep at it. I discovered how easy and clear the tools and UI were and how straightforward the whole layout was. There were a few of us in the Facebook group that regularly posted artwork for the challenges and we supported each other through it. I won't ever forget the wholesome and creative atmosphere in that group, especially for a newbie like me. We had a lot of fun and laughs in there. No one made fun of artwork and I remember it being a truly safe environment for boosting one's confidence and skill. It's made me eager to help out with the team.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those?**

I haven't done as much since I only recently joined, but I have created some gifs in the release notes to hopefully explain some of the tools and functions. I hope that they will be easy to understand for all users. I had never made gifs before but since I was comfortable with illustrating on Inkscape it made it easier to catch on.



**What's your favorite part of the 1.0 release?**

I love the new themes and the color palettes! I also like the split screen view. It's so useful to have both in one view!



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

I'd like for Inkscape to be able to import and open ai. and also eps files with the correct layers, gradients, blends and transparencies intact. It would really help with taking on projects from clientele who use those files exclusively. I'd like to see different color families, Pantone Color and gradients in the pop up palettes for print especially as well.



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

It can be intimidating at first, but look to your strengths and just ask what needs to be done. If there is something that you know you can do, do it to the best of your ability. It's amazing to work with people from all over the world whom we have never met but are all pulling together, for a worthy cause.



______________

Is there anything else you'd like to tell us?Keep up the great work!

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**

Facebook: ArtfulButterfly
Instagram: theartfulbutterfly

How can we contact you for details / clarifications?bennettsonia@gmail.com

