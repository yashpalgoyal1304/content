Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*dim, 26/04/2020 - 04:37*

*0.0.0.0*



**Contributor Interview: Sergei**



Headline 

**"I want my work tools to be cool and bug-free"**



**Please introduce yourself; what is your name and where in the world do you live?**

My name is Sergei, I live in St. Petersburg.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

I started contributing to the TexText extension. I'd been using it for years, but at one point I found it didn't work as expected. Then as a TexText developer I started noticing and fixing bugs in inkscape as well.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

Most of my contributions are connected to extensions subsystem.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

I want my work tools to be cool and bug-free. The community is very welcoming.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

I think extra dozen or two of skilled developers would change things dramatically, so be it.



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

Inkscape is quite a complex project. Feel free to ask in issues/chat for help/guidance.



**Is there anything else you'd like to tell us?**

It's hard to find chat.inkscape.org following google and inkscape.org web site. I believe it's most convenient way to get to community for new people, so it's kind of unfortunate.



______________________

How can we contact you for details / clarifications?

sizmailov

