Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*dim, 26/04/2020 - 01:26*

*0.0.0.0*



**Contributor Interview: Jonathan**



Headline 

**"Senior developers are more than willing to help newcomers"**



**Please introduce yourself; what is your name and where in the world do you live?**

Hi, I'm Jonathan and I live in Heidelberg, Germany.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

I've been using Inkscape for close to 10 years, and wanted to give something back. In December, I started filing some feature requests and helping to migrate issues from Launchpad to Gitlab. This quickly led to my introduction to the "Bug Wranglers" team.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I'm still primarily active in Inbox, helping to reproduce and triage bugs. User support is actually a part of that, since quite a few issues opened in inbox are support requests. (We can still learn from them though, since they indicate that a functionality is more convoluted than expected for a user). My activities in Inbox also include bug migration and QA testing of Inkscape.

Apart from that, I'm slowly getting to know the codebase, which allowed me to fix a few bugs myself.
Finally I've contributed to tutorials and their translations.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

Inkscape is a beautiful graphics software. I'm not a designer but an engineer, and as such, I probably use it in different ways than most of us - but it still supports my needs. The feature-packedness of Inkscape is its greatest strength, and also one of its weaknesses: features become difficult to find and error-prone. Making Inkscape even better in this regard is a huge motivation for me.

Inkscape is maintained by a small, but dynamic and sympathetic team. The first thing I noticed is how welcoming the community is - Senior developers are more than willing to help newcomers, and you can even get user feedback quickly on IRC.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

Most of my work went into bug management and QA. 1.0 has a lot of great new features, but they were severely undertested. When I joined, there was a huge amount of untriaged issues in inbox, which luckily has reduced to very few. Also, I think of the Launchpad bugtracker not as a steaming dung heap of forgotten bugs, but as a treasure trove of valuable user feedback and edge cases, waiting to be transformed into presentable shape.

Furthermore, I spent some time bringing the legacy Win32 theme in a presentable shape for 1.0.



**What's your favorite part of the 1.0 release?**

Best part? SVG2 text with SVG1.1 fallback. Best feature? The new LPEs. Most improved element: The XML editor.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

Constraint based editing. Engineers work a lot with CAD programs, where you can draw sketches and define the relative position of their elements with constraints - mathematical relations like "distance", "parallel", "coincidence", "tangential" and so on. The program's job is to keep these constraints fulfilled all the time if possible (this is the easy part), without transforming the drawing in a confusing way (this is the hard part). Allowing Inkscape to define constraints between arbitrary shapes, nodes, handles - that would be an absolute game changer. Think of it as Guides + Snapping on steroids :)



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

Don't be afraid to tap into the (perceived) domain of others. They probably welcome ideas from outside.



**Is there anything else you'd like to tell us?**

We need more people migrating bugs :)



_________

How can we contact you for details / clarifications?Nickname: jhofinger