Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 17:54*

*0.0.0.0*

**Contributor Interview: Duarte Ramos**



Headline 

**"The most rewarding part of this unexpected journey has been seeing the project mature"**



**Please introduce yourself; what is your name and where in the world do you live?**

Hello, my name is Duarte Ramos from Portugal



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

I was first contacted by Jared Meidal back in the distant year of 2013, when the then new Inkscape.org website was still a prototype. Back then I had lots of personal works shared in (the now defunct) Google+, and Jared mentioned featuring one of them as banner for the page. After some back and forth I ended up becoming involved with the Portuguese translation of the website.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I have mostly focused on the website and news translation work.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

The community has always been very welcoming and pleasant to deal with, all people involved seem really helpful and the atmosphere positive. I feel it also gives me some privileged insight into an Open Source project I have always had an interest in, and that I occasionally use professionally.



**What's your favorite part of the 1.0 release?**

Ah where to start? With so many goodies in the works its hard to pick just one. We have so many great new Live Path Effects, greatly improved UI, themes, improved tools and text editing.

If I have to pick one I'd go with improved performance, since occasional slowdowns were my major pain with previous versions.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

Well Inkscape is already pretty full featured, and complete, not really many complaints or wishes left.

The one thing I'd wave my magic wand at would probably have to be multi-page support. I mean the available toolset and features are already so great that the only thing left is to be able to use them for something akin to desktop publishing on multi page documents



**Is there anything else you'd like to tell us?**

The most rewarding part of this unexpected journey has been seeing the project mature from a relatively obscure amateur tool to one that can stand on its own against industry standards, along with all the members of this growing community.



___________________

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**

Twitter: @DuarteFRamos

How can we contact you for details / clarifications?duarte.framos@gmail.com