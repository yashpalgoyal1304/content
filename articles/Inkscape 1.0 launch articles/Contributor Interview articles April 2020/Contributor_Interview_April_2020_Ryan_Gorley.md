Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 18:29*

*0.0.0.0*



**Contributor Interview: Ryan Gorley**



Headline 

**"The friendships I've made keep me coming back"**



**Please introduce yourself; what is your name and where in the world do you live?**

I am Ryan Gorley and I'm from the Salt Lake City area in the United States.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

I am a marketing and creative professional. As the companies producing the software I depended upon started eliminating perpetual licensing and forcing people onto subscriptions, I started looking more seriously at ways to support alternatives. I ended up on a community run Inkscape forum and was invited to get more involved. It turned out to be a great time to do so. A lot of other people likewise wanted to contribute, but weren't programmers and didn't know how to help. Together we started the Vectors, which is an Inkscape team focused on outreach and promotion.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

My favorite contributions have probably been the least meaningful. I designed some fun banners promoting Inkscape as part of a "creative freedom suite" and have designed other creative collateral. I've been fortunate to attend a couple hackfests, where I was able to present a user's perspective on some of the features in development and long-term plans. I think what has had the most impact has probably been helping the Vectors establish a regular rhythm of meeting and contributing. We've met monthly, with few exceptions, for almost three years. During that time we've had a lot of great people get involved in different ways. I can't take credit for the work they've done, but I'm grateful to help facilitate the process.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

At first I was motivated by my hope to get Inkscape into more hands and to dispel the myth that professionals *must* use certain commercial tools. That still motivates me, but I think the friendships I've made keep me coming back in spite of increasing demands from family and work that can make it challenging. Inkscape has attracted really great people who write the code and contribute in countless other ways. They deserve someone cheering them on and making their lives easier.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

I can't take much credit for 1.0, but I was in the room when the decision was made to name it 1.0!



**What's your favorite part of the 1.0 release?**

For those familiar with Inkscape already, 1.0 will feel like a natural evolution. The bump from 0.92.x is largely an overdue symbolic gesture that says the software is all grown up, but there are some nice improvements. I'm pretty exciting about the changes to how typography is handled, as this is a part of my work every day. I'm also excited about GTK+ 3 and the potential it has to offer, not the least of which being a more accessible macOS build. It's hard to overstate how ubiquitous Apple products are among creative professionals. While the macOS build is still in beta pending some work to improve performance, it is a big step towards liberating a bunch of software captives like my prior self. :)



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

If I could wave a magic wand I would love to see better CMYK support within Inkscape. It's a big project that we've had a hard time getting off the ground, but it would make a huge difference for a lot of people. As a designer I'm a bit more critical of things like interface design, consistent branding and quality presentation. I think appearances do have an impact on perception and I would like to see continued improvement on how we present Inkscape in order to do justice to the software and the amazing things it can create. While I'm waving magic wands, I'd like to find a way to get more developers involved and reward them for the great work they do. There's more, but I'll stop wishing!



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

My best advice to someone who wants to get involved with Inkscape, or any open source project like it, is to be patient. When I first got involved I had big plans and I bumped up against a lot of obstacles that I didn't understand because I had never worked in an environment quite like this. Things take time, and community buy-in is really important. Get involved and be passionate, but also be patient and be kind.



**Is there anything else you'd like to tell us?**

I hope people will get involved in Inkscape, or an alternative free-software project they feel passionate about. There are people all over the world who have the opportunity to express themselves, learn how to design, and make a living because of this software that is made freely available to them. There are a lot of good things one can do with their life. If at all possible, supporting a project like this should be one of them.



______________

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**

Mastodon: @ryangorley@mastodon.social
Twitter: @ryangorley
Facebook:
Instagram:

How can we contact you for details / clarifications?ryan@freehive.com