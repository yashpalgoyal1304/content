## Synopsis
A short description of the article here.

## Audience
Who will be interested in this article here.

## Title
Proposed title for the article here.

## Author
Suggested author of the article here.

## File
[Add link to content repo file https://gitlab.com/inkscape/vectors/content/master/articles/...]

## Publication Channels
<!-- remove spaces between @ and name when it's time to distribute the tasks -->

* [ ] Website
* [ ] Twitter post ( @ ryangorley )
* [ ] Facebook group post ( @ t1mj0nes )
* [ ] Mastodon post ( @ ryangorley )
* [ ] Forums ( @ brynn or @ Lazur )
* [ ] google+ ( @ prokoudine, @ ryangorley or @ CRogers )
* [ ] deviantArt ( @ doctormo or @ Lazur )
* [ ] openClipart ( @ Lazur )
* [ ] facebook (@ rsterbal )
