# Font Licenses

[Euphoria Script](https://www.typesenses.com/euphoria): [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web)

[Linux Libertine Display](http://libertine-fonts.org/):  [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web) and [General Public License](http://www.gnu.org/licenses/gpl.html)

[Ubuntu](https://design.ubuntu.com/font/): Ubuntu Font Licence (included in zip file)