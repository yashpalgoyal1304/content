# SLIDE 4
![Why I selected LPE as a way to start contributing to Inkscape coding??](previews/04.png "Why I selected LPE as a way to start contributing to Inkscape coding??")
## SLIDE CONTENT
### Why select LPEs as a way to start contributing to Inkscape coding?
It was a good way to start because the LPE code was isolated, so I didn't need  
to understand the whole codebase.
## SLIDE NOTES
LPE is a good way to start coding for Inkscape because the LPE code is isolated, 
so you don't need to understand the whole codebase.  
It gives you access to the power of C++ to do a lot of cool things. The tough part is it needs  
to ship with Inkscape, but it is much more powerful and faster than traditional python extensions.
## SLIDE NOTES PRONOUNCE
...

